package com.tmdb.common.usecase

import com.tmdb.api.repository.FavouriteMoviesRepository
import com.tmdb.di.qualifiers.BackgroundScheduler
import com.tmdb.di.qualifiers.ForegroundScheduler
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class IsFavouriteMovieUseCase @Inject constructor(
    @BackgroundScheduler private val backgroundScheduler: Scheduler,
    @ForegroundScheduler private val foregroundScheduler: Scheduler,
    private val favouriteMoviesRepository: FavouriteMoviesRepository
) {
    fun execute(movieId: Int): Single<Boolean> {
        return favouriteMoviesRepository.loadFavouriteMovie(movieId)
            .subscribeOn(backgroundScheduler)
            .observeOn(foregroundScheduler)
            .flatMapSingle {
                Single.just(true)
            }.switchIfEmpty(Single.just(false))
    }
}
