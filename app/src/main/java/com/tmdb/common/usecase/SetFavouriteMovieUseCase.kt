package com.tmdb.common.usecase

import com.tmdb.api.repository.FavouriteMoviesRepository
import com.tmdb.db.entity.FavouriteMovie
import com.tmdb.di.qualifiers.BackgroundScheduler
import com.tmdb.di.qualifiers.ForegroundScheduler
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SetFavouriteMovieUseCase @Inject constructor(
    @BackgroundScheduler private val backgroundScheduler: Scheduler,
    @ForegroundScheduler private val foregroundScheduler: Scheduler,
    private val favouriteMoviesRepository: FavouriteMoviesRepository
) {
    fun execute(currentValue: Boolean, movieId: Int): Single<Boolean> {
        return when (currentValue) {
            true -> {
                favouriteMoviesRepository.deleteFavouriteMovies(FavouriteMovie(movieId))
                    .subscribeOn(backgroundScheduler)
                    .observeOn(foregroundScheduler)
                    .toSingle { false }
            }
            else -> {
                favouriteMoviesRepository.insertFavouriteMovies(FavouriteMovie(movieId))
                    .subscribeOn(backgroundScheduler)
                    .observeOn(foregroundScheduler)
                    .toSingle { true }
            }

        }
    }
}
