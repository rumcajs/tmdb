package com.tmdb.common.helper

import com.tmdb.R

fun getFavouriteImageResource(favourite: Boolean?): Int = when (favourite) {
    true -> R.drawable.ic_star_filled
    else -> R.drawable.ic_star
}
