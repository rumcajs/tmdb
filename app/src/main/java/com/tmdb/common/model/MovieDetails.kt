package com.tmdb.common.model

data class MovieDetails(
    val title: String,
    val overview: String,
    val averageVote: String,
    val releaseDate: String,
    val photoPath: String
)
