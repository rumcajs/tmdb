package com.tmdb.common.model

data class Movie(
    val id: Int,
    val title: String,
)
