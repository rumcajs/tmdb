package com.tmdb.movieslist.usecase

import com.tmdb.api.model.MoviesResponseDto
import com.tmdb.api.repository.SearchRepository
import com.tmdb.di.qualifiers.BackgroundScheduler
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SearchMovieUseCase @Inject constructor(
    @BackgroundScheduler private val backgroundScheduler: Scheduler,
    private val searchRepository: SearchRepository
) {
    fun execute(query: String, nextPageNumber: Int): Single<MoviesResponseDto> =
        searchRepository.searchMovie(query, nextPageNumber)
            .subscribeOn(backgroundScheduler)
}
