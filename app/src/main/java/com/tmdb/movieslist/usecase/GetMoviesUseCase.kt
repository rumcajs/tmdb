package com.tmdb.movieslist.usecase

import com.tmdb.api.model.MoviesResponseDto
import com.tmdb.api.repository.MoviesRepository
import com.tmdb.di.qualifiers.BackgroundScheduler
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetMoviesUseCase @Inject constructor(
    @BackgroundScheduler private val backgroundScheduler: Scheduler,
    private val moviesRepository: MoviesRepository
) {
    fun execute(nextPageNumber: Int): Single<MoviesResponseDto> =
        moviesRepository.getNowPlaying(nextPageNumber)
            .subscribeOn(backgroundScheduler)
}
