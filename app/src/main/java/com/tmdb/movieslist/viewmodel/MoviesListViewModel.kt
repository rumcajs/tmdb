package com.tmdb.movieslist.viewmodel

import androidx.lifecycle.*
import androidx.paging.*
import com.tmdb.adapter.recyclerview.item.MovieItemViewModel
import com.tmdb.adapter.recyclerview.paging.MoviesPagingSourceFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

private const val PAGE_SIZE = 20

@HiltViewModel
class MoviesListViewModel @Inject constructor(
    private val moviesPagingSourceFactory: MoviesPagingSourceFactory,
) : ViewModel() {

    val query = MutableLiveData("")
    val moviesPagingData = query.distinctUntilChanged().switchMap { query ->
        setUpPagingData(query)
    }

    private fun setUpPagingData(query: String): LiveData<PagingData<MovieItemViewModel>> {
        val pager = Pager(PagingConfig(PAGE_SIZE)) {
            moviesPagingSourceFactory.createMoviesPagingSource(query)
        }
        return pager.liveData.cachedIn(viewModelScope)
    }
}
