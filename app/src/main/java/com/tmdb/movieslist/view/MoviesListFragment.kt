package com.tmdb.movieslist.view

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import androidx.paging.PagingData
import com.tmdb.R
import com.tmdb.adapter.recyclerview.MoviesRecyclerViewAdapter
import com.tmdb.adapter.recyclerview.MoviesRecyclerViewAdapterFactory
import com.tmdb.common.model.Movie
import com.tmdb.databinding.FragmentMoviesListBinding
import com.tmdb.moviedetails.args.MovieDetailsArgs
import com.tmdb.movieslist.viewmodel.MoviesListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MoviesListFragment : Fragment() {

    @Inject
    lateinit var adapterFactory: MoviesRecyclerViewAdapterFactory

    private val moviesListViewModel by viewModels<MoviesListViewModel>()
    private var binding: FragmentMoviesListBinding? = null
    private lateinit var adapter: MoviesRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        return FragmentMoviesListBinding.inflate(inflater, container, false).apply {
            viewModel = moviesListViewModel
            lifecycleOwner = viewLifecycleOwner
        }.also {
            binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setUpToolbar(view)
        setUpMenu()
        setUpRecyclerView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun setUpToolbar(view: View) {
        val navController = Navigation.findNavController(view)
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        getToolbar()?.setupWithNavController(navController, appBarConfiguration)
    }

    private fun setUpMenu() {
        getToolbar()?.apply {
            inflateMenu(R.menu.menu_search)
            (getSearchItem(menu).actionView as SearchView).apply {
                queryHint = resources.getString(R.string.search)
                setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        onSearchTextChange(newText)
                        return true
                    }
                })
            }
        }
    }

    private fun setUpRecyclerView() {
        adapter = adapterFactory.createMoviesRecyclerViewAdapter(viewLifecycleOwner).apply {
            onItemClick = { view, data ->
                onItemClick(view, data)
            }
        }
        getRecyclerView()?.adapter = adapter

        moviesListViewModel.moviesPagingData.observe(viewLifecycleOwner) { pagingData ->
            adapter.submitData(viewLifecycleOwner.lifecycle, pagingData)
        }
    }

    private fun onSearchTextChange(newText: String?) {
        newText?.let { text ->
            adapter.submitData(viewLifecycleOwner.lifecycle, PagingData.empty())
            moviesListViewModel.query.value = text
        }
    }

    private fun onItemClick(view: View, data: Movie) {
        view.findNavController().navigate(
            MoviesListFragmentDirections.actionMoviesListFragmentToMovieDetailsFragment(
                MovieDetailsArgs(data.id)
            )
        )
    }

    private fun getToolbar() = binding?.toolbar

    private fun getSearchItem(menu: Menu) = menu.findItem(R.id.searchBar)

    private fun getRecyclerView() = binding?.recyclerView
}
