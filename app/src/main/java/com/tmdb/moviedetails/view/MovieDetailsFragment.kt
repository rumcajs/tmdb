package com.tmdb.moviedetails.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.tmdb.R
import com.tmdb.common.helper.getFavouriteImageResource
import com.tmdb.databinding.FragmentMovieDetailsBinding
import com.tmdb.moviedetails.viewmodel.MovieDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieDetailsFragment : Fragment() {

    private val movieDetailsViewModel by viewModels<MovieDetailsViewModel>()
    private var binding: FragmentMovieDetailsBinding? = null
    private val args: MovieDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentMovieDetailsBinding.inflate(inflater, container, false).apply {
            viewModel = movieDetailsViewModel
            lifecycleOwner = viewLifecycleOwner
        }.also {
            binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setUpToolbar(view)
        setUpMenu()
        getData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    private fun setUpToolbar(view: View) {
        val navController = Navigation.findNavController(view)
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        getToolbar()?.apply {
            setupWithNavController(navController, appBarConfiguration)
            setOnMenuItemClickListener { item ->
                return@setOnMenuItemClickListener when(item.itemId) {
                    R.id.favouriteItem -> {
                        movieDetailsViewModel.onFavouriteClick()
                        true
                    }
                    else -> true
                }
            }
        }
    }

    private fun setUpMenu() {
        getToolbar()?.apply {
            inflateMenu(R.menu.menu_favourite)
        }
    }

    private fun getData() {
        with(movieDetailsViewModel) {
            movieId.value = args.movieDetailsArgs.movieId
            favourite.observe(viewLifecycleOwner) {
                getFavouriteItem()?.setIcon(getFavouriteImageResource(it))
            }
        }
    }

    private fun getToolbar() = binding?.toolbar

    private fun getFavouriteItem() = getToolbar()?.menu?.findItem(R.id.favouriteItem)
}
