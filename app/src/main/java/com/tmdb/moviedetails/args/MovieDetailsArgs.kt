package com.tmdb.moviedetails.args

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieDetailsArgs(val movieId: Int) : Parcelable
