package com.tmdb.moviedetails.viewmodel

import androidx.lifecycle.*
import com.tmdb.common.model.MovieDetails
import com.tmdb.common.usecase.IsFavouriteMovieUseCase
import com.tmdb.common.usecase.SetFavouriteMovieUseCase
import com.tmdb.moviedetails.usecase.GetMovieDetailsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import javax.inject.Inject

@HiltViewModel
class MovieDetailsViewModel @Inject constructor(
    private val getMovieDetailsUseCase: GetMovieDetailsUseCase,
    private val isFavouriteMovieUseCase: IsFavouriteMovieUseCase,
    private val setFavouriteMovieUseCase: SetFavouriteMovieUseCase
) : ViewModel() {

    val movieId = MutableLiveData<Int>()
    val movieDetails: LiveData<MovieDetails>
        get() = _movieDetails
    val favourite: LiveData<Boolean>
        get() = _favourite

    private val _movieDetails = MutableLiveData<MovieDetails>()
    private val _favourite = MutableLiveData(false)
    private val movieIdObserver = Observer<Int> { id ->
        getMovieDetails(id)
        getFavourite(id)
    }
    private val disposable = CompositeDisposable()
    private var setFavouriteDisposable: Disposable? = null

    init {
        movieId.distinctUntilChanged().observeForever(movieIdObserver)
    }

    fun onFavouriteClick() {
        setFavouriteDisposable?.dispose()
        movieId.value?.let { id ->
            _favourite.value?.let { value ->
                setFavouriteDisposable = setFavouriteMovieUseCase.execute(value, id)
                    .subscribe({ newValue ->
                        _favourite.value = newValue
                    }, {

                    })
            }
        }
    }

    private fun getMovieDetails(id: Int) {
        disposable.add(
            getMovieDetailsUseCase.execute(id).subscribe({ movieDetails ->
                _movieDetails.value = movieDetails
            }, {

            })
        )
    }

    private fun getFavourite(id: Int) {
        disposable.add(
            isFavouriteMovieUseCase.execute(id).subscribe({
                _favourite.value = it
            }, {

            })
        )
    }

    override fun onCleared() {
        super.onCleared()
        movieId.removeObserver(movieIdObserver)
        disposable.dispose()
        setFavouriteDisposable?.dispose()
    }
}
