package com.tmdb.moviedetails.usecase

import com.tmdb.api.model.mapper.mapMovieDetailsDtoToMovieDetails
import com.tmdb.api.repository.MoviesRepository
import com.tmdb.di.qualifiers.BackgroundScheduler
import com.tmdb.di.qualifiers.ForegroundScheduler
import com.tmdb.common.model.MovieDetails
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetMovieDetailsUseCase @Inject constructor(
    @BackgroundScheduler private val backgroundScheduler: Scheduler,
    @ForegroundScheduler private val foregroundScheduler: Scheduler,
    private val moviesRepository: MoviesRepository
) {
    fun execute(movieId: Int): Flowable<MovieDetails> =
        moviesRepository.getMovieDetails(movieId)
            .subscribeOn(backgroundScheduler)
            .observeOn(foregroundScheduler)
            .concatMap { response ->
                Single.just(mapMovieDetailsDtoToMovieDetails(response))
            }.toFlowable().onBackpressureLatest()
}
