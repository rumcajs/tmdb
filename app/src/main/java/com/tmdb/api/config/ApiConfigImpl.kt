package com.tmdb.api.config

import javax.inject.Inject

private const val API_BASE_URL = "https://api.themoviedb.org/3/"
private const val IMAGES_BASE_URL = "https://image.tmdb.org/t/p/original/"

private const val API_KEY =
    "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5MDA1OGM0ODY5MjMwZWEzNjc3MDhjMGEyOWUyZWI2MSIsInN1YiI6IjYxOWQwN2UwMzVkYjQ1MDA0MzM0ZWYxMSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.v9G8GtTxw2guD3QjdZ60oqkj3XhnPlZQ9Yjkqnq2Ygc"

private val HEADERS = mapOf(
    "Authorization" to "Bearer $API_KEY",
    "Content-Type" to "application/json;charset=utf-8"
)

private const val LANGUAGE = "en-US"

class ApiConfigImpl @Inject constructor() : ApiConfig {

    override fun getApiBaseUrl(): String = API_BASE_URL

    override fun getImagesBaseUrl(): String = IMAGES_BASE_URL

    override fun getDefaultHeaders(): Map<String, String> = HEADERS

    override fun getApiLanguage(): String = LANGUAGE
}
