package com.tmdb.api.config

interface ApiConfig {

    fun getApiBaseUrl(): String
    fun getImagesBaseUrl(): String
    fun getDefaultHeaders(): Map<String, String>
    fun getApiLanguage(): String
}