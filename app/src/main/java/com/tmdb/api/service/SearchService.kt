package com.tmdb.api.service

import com.tmdb.api.model.MoviesResponseDto
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Query

interface SearchService {

    @GET("search/movie")
    fun searchMovie(
        @HeaderMap headers: Map<String, String>,
        @Query("language") language: String,
        @Query("query") query: String,
        @Query("page") page: Int
    ): Single<MoviesResponseDto>
}
