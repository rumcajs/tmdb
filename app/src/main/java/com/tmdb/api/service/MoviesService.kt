package com.tmdb.api.service

import com.tmdb.api.model.MovieDetailsDto
import com.tmdb.api.model.MoviesResponseDto
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.HeaderMap
import retrofit2.http.Path
import retrofit2.http.Query

interface MoviesService {

    @GET("movie/now_playing")
    fun getNowPlaying(
        @HeaderMap headers: Map<String, String>,
        @Query("language") language: String,
        @Query("page") page: Int
    ): Single<MoviesResponseDto>

    @GET("movie/{movie_id}")
    fun getMovieDetails(
        @HeaderMap headers: Map<String, String>,
        @Path("movie_id") movieId: Int,
        @Query("language") language: String
    ): Single<MovieDetailsDto>
}
