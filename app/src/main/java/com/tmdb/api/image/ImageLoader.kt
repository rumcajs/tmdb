package com.tmdb.api.image

import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.tmdb.api.config.ApiConfig
import java.lang.StringBuilder
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ImageLoader @Inject constructor(private val apiConfig: ApiConfig) {

    fun getImage(imagePath: String, imageView: ImageView) {
        Picasso.get().load(getImageUrl(imagePath)).into(imageView)
    }

    private fun getImageUrl(imagePath: String): String =
        StringBuilder(apiConfig.getImagesBaseUrl()).append(imagePath).toString()
}
