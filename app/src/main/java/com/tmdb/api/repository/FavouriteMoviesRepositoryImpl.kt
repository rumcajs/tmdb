package com.tmdb.api.repository

import com.tmdb.db.dao.FavouriteMovieDao
import com.tmdb.db.entity.FavouriteMovie
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import javax.inject.Inject

class FavouriteMoviesRepositoryImpl @Inject constructor(
    private val favouriteMovieDao: FavouriteMovieDao
) : FavouriteMoviesRepository {

    override fun insertFavouriteMovies(vararg favouriteMovies: FavouriteMovie): Completable =
        favouriteMovieDao.insertFavouriteMovies(*favouriteMovies)

    override fun deleteFavouriteMovies(vararg favouriteMovies: FavouriteMovie): Completable =
        favouriteMovieDao.deleteFavouriteMovies(*favouriteMovies)

    override fun loadFavouriteMovie(id: Int): Maybe<FavouriteMovie> =
        favouriteMovieDao.loadFavouriteMovie(id)
}
