package com.tmdb.api.repository

import com.tmdb.api.model.MoviesResponseDto
import io.reactivex.rxjava3.core.Single

interface SearchRepository {

    fun searchMovie(query: String, page: Int): Single<MoviesResponseDto>
}
