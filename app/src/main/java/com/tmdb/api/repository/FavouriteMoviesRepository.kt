package com.tmdb.api.repository

import androidx.room.*
import com.tmdb.db.entity.FavouriteMovie
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe

@Dao
interface FavouriteMoviesRepository {

    fun insertFavouriteMovies(vararg favouriteMovies: FavouriteMovie): Completable

    fun deleteFavouriteMovies(vararg favouriteMovies: FavouriteMovie): Completable

    fun loadFavouriteMovie(id: Int): Maybe<FavouriteMovie>
}
