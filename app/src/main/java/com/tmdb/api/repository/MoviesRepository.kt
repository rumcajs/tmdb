package com.tmdb.api.repository

import com.tmdb.api.model.MovieDetailsDto
import com.tmdb.api.model.MoviesResponseDto
import io.reactivex.rxjava3.core.Single

interface MoviesRepository {

    fun getNowPlaying(page: Int): Single<MoviesResponseDto>

    fun getMovieDetails(movieId: Int): Single<MovieDetailsDto>
}
