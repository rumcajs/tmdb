package com.tmdb.api.repository

import com.tmdb.api.config.ApiConfig
import com.tmdb.api.model.MoviesResponseDto
import com.tmdb.api.service.SearchService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SearchRepositoryImpl @Inject constructor(
    private val apiConfig: ApiConfig,
    private val searchService: SearchService
) : SearchRepository {

    override fun searchMovie(query: String, page: Int): Single<MoviesResponseDto> =
        searchService.searchMovie(
            apiConfig.getDefaultHeaders(),
            apiConfig.getApiLanguage(),
            query,
            page
        )
}
