package com.tmdb.api.repository

import com.tmdb.api.config.ApiConfig
import com.tmdb.api.model.MovieDetailsDto
import com.tmdb.api.model.MoviesResponseDto
import com.tmdb.api.service.MoviesService
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class MoviesRepositoryImpl @Inject constructor(
    private val apiConfig: ApiConfig,
    private val moviesService: MoviesService
) : MoviesRepository {

    override fun getNowPlaying(page: Int): Single<MoviesResponseDto> =
        moviesService.getNowPlaying(
            apiConfig.getDefaultHeaders(),
            apiConfig.getApiLanguage(),
            page
        )

    override fun getMovieDetails(movieId: Int): Single<MovieDetailsDto> =
        moviesService.getMovieDetails(
            apiConfig.getDefaultHeaders(),
            movieId,
            apiConfig.getApiLanguage()
        )
}
