package com.tmdb.api.model.mapper

import com.tmdb.api.model.MovieDetailsDto
import com.tmdb.common.model.MovieDetails

fun mapMovieDetailsDtoToMovieDetails(dto: MovieDetailsDto) = with(dto) {
    MovieDetails(title, overview, vote.toString(), releaseDate, photoPath)
}
