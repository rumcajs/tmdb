package com.tmdb.api.model.mapper

import com.tmdb.api.model.MovieDto
import com.tmdb.common.model.Movie

fun mapMovieDtoToMovie(dto: MovieDto) = with(dto) {
    Movie(id, title)
}
