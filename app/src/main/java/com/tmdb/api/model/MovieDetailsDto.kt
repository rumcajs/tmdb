package com.tmdb.api.model

import com.google.gson.annotations.SerializedName

data class MovieDetailsDto(
    @SerializedName("title")
    val title: String,
    @SerializedName("overview")
    val overview: String,
    @SerializedName("vote_average")
    val vote: Float,
    @SerializedName("release_date")
    val releaseDate: String,
    @SerializedName("backdrop_path")
    val photoPath: String
)
