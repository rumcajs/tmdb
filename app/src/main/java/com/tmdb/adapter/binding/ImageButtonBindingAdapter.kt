package com.tmdb.adapter.binding

import android.widget.ImageButton
import androidx.databinding.BindingAdapter
import com.tmdb.common.helper.getFavouriteImageResource

@BindingAdapter("setFavourite")
fun setFavourite(imageButton: ImageButton, favourite: Boolean?) {
    imageButton.setImageResource(getFavouriteImageResource(favourite))
}
