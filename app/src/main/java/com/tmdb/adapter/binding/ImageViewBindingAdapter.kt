package com.tmdb.adapter.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.tmdb.api.image.ImageLoader
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ImageViewBindingAdapter @Inject constructor(private val imageLoader: ImageLoader) {

    @BindingAdapter("loadMovieImage")
    fun loadMovieImage(view: ImageView, imagePath: String?) {
        imagePath?.let { path ->
            imageLoader.getImage(path, view)
        }
    }
}
