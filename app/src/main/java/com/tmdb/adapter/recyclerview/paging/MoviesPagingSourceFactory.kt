package com.tmdb.adapter.recyclerview.paging

import dagger.assisted.AssistedFactory

@AssistedFactory
interface MoviesPagingSourceFactory {

    fun createMoviesPagingSource(query: String): MoviesPagingSource
}
