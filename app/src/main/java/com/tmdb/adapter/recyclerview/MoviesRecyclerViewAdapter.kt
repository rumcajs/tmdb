package com.tmdb.adapter.recyclerview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.*
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tmdb.R
import com.tmdb.adapter.recyclerview.holder.MoviesViewHolder
import com.tmdb.adapter.recyclerview.item.MovieItemViewModel
import com.tmdb.common.model.Movie
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject

class MoviesRecyclerViewAdapter @AssistedInject constructor(
    diffCallback: DiffUtil.ItemCallback<MovieItemViewModel>,
    @Assisted
    private val lifecycleOwner: LifecycleOwner
) : PagingDataAdapter<MovieItemViewModel, MoviesViewHolder>(diffCallback) {

    var onItemClick: ((View, Movie) -> Unit)? = null
    private var recyclerView: RecyclerView? = null

    init {
        handleViewLifecycle()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding?>(
            LayoutInflater.from(parent.context),
            R.layout.item_movie,
            parent,
            false
        )
        return MoviesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        getItem(position)?.let { movieViewHolder ->
            holder.bind(movieViewHolder.apply {
                onBind()
                clickAction = { view, data ->
                    onItemClick?.invoke(view, data)
                }
            })
        }
    }

    override fun onViewAttachedToWindow(holder: MoviesViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.onAttach()
    }

    override fun onViewDetachedFromWindow(holder: MoviesViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.onDetach()
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        this.recyclerView = null
    }

    private fun handleViewLifecycle() {
        lifecycleOwner.lifecycle.addObserver(object : LifecycleEventObserver {
            override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
                when (event) {
                    Lifecycle.Event.ON_START -> onStart()
                    Lifecycle.Event.ON_STOP -> onStop()
                    Lifecycle.Event.ON_DESTROY -> onDestroy()
                    else -> Unit
                }
            }
        })
    }

    private fun onStart() {
        recyclerView?.run {
            (layoutManager as? LinearLayoutManager)?.let { linearLayoutManager ->
                val first = linearLayoutManager.findFirstVisibleItemPosition()
                val last = linearLayoutManager.findLastVisibleItemPosition()
                if (first in 0..last) {
                    for (i in first..last) {
                        findViewHolderForAdapterPosition(i)?.let { holder ->
                            (holder as? MoviesViewHolder)?.onAttach()
                        }
                    }
                }
            }
        }
    }

    private fun onStop() {
        recyclerView?.let { parent ->
            for (i in 0 until parent.childCount) {
                parent.getChildAt(i)?.let { child ->
                    (parent.getChildViewHolder(child) as? MoviesViewHolder)?.onDetach()
                }
            }
        }
    }

    private fun onDestroy() {
        recyclerView?.let { parent ->
            for (i in 0 until parent.childCount) {
                parent.getChildAt(i)?.let { child ->
                    (parent.getChildViewHolder(child) as? MoviesViewHolder)?.onDestroy()
                }
            }
        }
    }
}
