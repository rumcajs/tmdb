package com.tmdb.adapter.recyclerview

import androidx.lifecycle.LifecycleOwner
import dagger.assisted.AssistedFactory

@AssistedFactory
interface MoviesRecyclerViewAdapterFactory {

    fun createMoviesRecyclerViewAdapter(lifecycleOwner: LifecycleOwner): MoviesRecyclerViewAdapter
}
