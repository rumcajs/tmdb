package com.tmdb.adapter.recyclerview.paging

import androidx.paging.PagingState
import androidx.paging.rxjava3.RxPagingSource
import com.tmdb.adapter.recyclerview.item.MovieItemViewModel
import com.tmdb.adapter.recyclerview.item.MovieItemViewModelFactory
import com.tmdb.api.model.MoviesResponseDto
import com.tmdb.api.model.mapper.mapMovieDtoToMovie
import com.tmdb.movieslist.usecase.GetMoviesUseCase
import com.tmdb.movieslist.usecase.SearchMovieUseCase
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import io.reactivex.rxjava3.core.Single

private const val INITIAL_PAGE = 1

class MoviesPagingSource @AssistedInject constructor(
    private val getMoviesUseCase: GetMoviesUseCase,
    private val searchMovieUseCase: SearchMovieUseCase,
    private val movieItemViewModelFactory: MovieItemViewModelFactory,
    @Assisted
    private val query: String
) : RxPagingSource<Int, MovieItemViewModel>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, MovieItemViewModel>> {
        val nextPageNumber = params.key ?: INITIAL_PAGE

        return if (query.isBlank()) {
            getMoviesUseCase.execute(nextPageNumber)
                .map(this::toLoadResult)
                .onErrorReturn { LoadResult.Error(it) }
        } else {
            searchMovieUseCase.execute(query, nextPageNumber)
                .map(this::toLoadResult)
                .onErrorReturn { LoadResult.Error(it) }
        }
    }

    override fun getRefreshKey(state: PagingState<Int, MovieItemViewModel>): Int? =
        state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }

    private fun toLoadResult(response: MoviesResponseDto): LoadResult<Int, MovieItemViewModel> =
        LoadResult.Page(
            data = response.results.map { movieDto ->
                movieItemViewModelFactory.createMovieItemViewModel(mapMovieDtoToMovie(movieDto))
            },
            prevKey = null,
            nextKey = if (response.results.isNotEmpty()) {
                response.page + 1
            } else {
                null
            }
        )
}
