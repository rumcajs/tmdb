package com.tmdb.adapter.recyclerview.item

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tmdb.common.model.Movie
import com.tmdb.common.usecase.IsFavouriteMovieUseCase
import com.tmdb.common.usecase.SetFavouriteMovieUseCase
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import io.reactivex.rxjava3.disposables.Disposable

class MovieItemViewModel @AssistedInject constructor(
    private val isFavouriteMovieUseCase: IsFavouriteMovieUseCase,
    private val setFavouriteMovieUseCase: SetFavouriteMovieUseCase,
    @Assisted
    val data: Movie
) {

    val favourite: LiveData<Boolean>
        get() = _favourite
    var clickAction: ((View, Movie) -> Unit)? = null

    private val _favourite = MutableLiveData(false)
    private var isFavouriteDisposable: Disposable? = null
    private var setFavouriteDisposable: Disposable? = null

    fun onBind() {
        isFavouriteDisposable = isFavouriteMovieUseCase.execute(data.id).subscribe({
            _favourite.value = it
        }, {

        })
    }

    fun onClick(view: View) {
        clickAction?.invoke(view, data)
    }

    fun onFavouriteClick() {
        setFavouriteDisposable?.dispose()
        _favourite.value?.let { value ->
            setFavouriteDisposable = setFavouriteMovieUseCase.execute(value, data.id)
                .subscribe({ newValue ->
                    _favourite.value = newValue
                }, {

                })
        }
    }

    fun onCleared() {
        isFavouriteDisposable?.dispose()
        setFavouriteDisposable?.dispose()
    }
}
