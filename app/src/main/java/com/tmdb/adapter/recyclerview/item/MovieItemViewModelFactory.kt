package com.tmdb.adapter.recyclerview.item

import com.tmdb.common.model.Movie
import dagger.assisted.AssistedFactory

@AssistedFactory
interface MovieItemViewModelFactory {

    fun createMovieItemViewModel(data: Movie): MovieItemViewModel
}
