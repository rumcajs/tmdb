package com.tmdb.adapter.recyclerview.comparator

import androidx.recyclerview.widget.DiffUtil
import com.tmdb.adapter.recyclerview.item.MovieItemViewModel
import javax.inject.Inject

class MovieComparator @Inject constructor() : DiffUtil.ItemCallback<MovieItemViewModel>() {

    override fun areItemsTheSame(
        oldItem: MovieItemViewModel,
        newItem: MovieItemViewModel
    ): Boolean = oldItem.data.id == newItem.data.id

    override fun areContentsTheSame(
        oldItem: MovieItemViewModel,
        newItem: MovieItemViewModel
    ): Boolean = oldItem.data == newItem.data
}
