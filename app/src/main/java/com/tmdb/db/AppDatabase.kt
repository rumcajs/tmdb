package com.tmdb.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tmdb.db.dao.FavouriteMovieDao
import com.tmdb.db.entity.FavouriteMovie

@Database(entities = [FavouriteMovie::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun favouriteMovieDao(): FavouriteMovieDao
}
