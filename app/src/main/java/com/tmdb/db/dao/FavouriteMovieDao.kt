package com.tmdb.db.dao

import androidx.room.*
import com.tmdb.db.entity.FavouriteMovie
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe

@Dao
interface FavouriteMovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavouriteMovies(vararg favouriteMovies: FavouriteMovie): Completable

    @Delete
    fun deleteFavouriteMovies(vararg favouriteMovies: FavouriteMovie): Completable

    @Query("SELECT * FROM favourite_movies WHERE id = :id")
    fun loadFavouriteMovie(id: Int): Maybe<FavouriteMovie>
}
