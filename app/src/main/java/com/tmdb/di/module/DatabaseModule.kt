package com.tmdb.di.module

import android.content.Context
import androidx.room.Room
import com.tmdb.db.AppDatabase
import com.tmdb.db.dao.FavouriteMovieDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase =
        Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "AppDatabase"
        ).build()

    @Provides
    fun provideFavouriteMovieDao(appDatabase: AppDatabase): FavouriteMovieDao =
        appDatabase.favouriteMovieDao()
}
