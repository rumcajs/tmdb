package com.tmdb.di.module

import com.tmdb.api.config.ApiConfig
import com.tmdb.api.config.ApiConfigImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ApiConfigModule {

    @Binds
    @Singleton
    abstract fun bindApiConfig(impl: ApiConfigImpl): ApiConfig
}
