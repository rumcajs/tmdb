package com.tmdb.di.module

import androidx.recyclerview.widget.DiffUtil
import com.tmdb.adapter.recyclerview.comparator.MovieComparator
import com.tmdb.adapter.recyclerview.item.MovieItemViewModel
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ComparatorModule {

    @Binds
    @Singleton
    abstract fun bindMovieComparator(impl: MovieComparator): DiffUtil.ItemCallback<MovieItemViewModel>
}
