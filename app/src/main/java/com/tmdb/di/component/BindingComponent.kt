package com.tmdb.di.component

import com.tmdb.di.scope.BindingScoped
import dagger.hilt.DefineComponent
import dagger.hilt.components.SingletonComponent

@BindingScoped
@DefineComponent(parent = SingletonComponent::class)
interface BindingComponent
