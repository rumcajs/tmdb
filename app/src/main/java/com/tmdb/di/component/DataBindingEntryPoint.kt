package com.tmdb.di.component

import androidx.databinding.DataBindingComponent
import com.tmdb.adapter.binding.ImageViewBindingAdapter
import com.tmdb.di.scope.BindingScoped
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn

@EntryPoint
@BindingScoped
@InstallIn(BindingComponent::class)
interface DataBindingEntryPoint: DataBindingComponent {

    @BindingScoped
    override fun getImageViewBindingAdapter() : ImageViewBindingAdapter
}
