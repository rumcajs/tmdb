package com.tmdb.di.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class BackgroundScheduler
