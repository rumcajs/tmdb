package com.tmdb.di.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.BINARY)
annotation class BindingScoped
